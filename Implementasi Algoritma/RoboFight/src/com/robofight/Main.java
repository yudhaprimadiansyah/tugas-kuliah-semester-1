package com.robofight;
import java.util.Scanner;
import java.util.Random;
public class Main {

    public static void main(String[] args) {
        String nama1 = "", nama2 = "";
        int Evasion1 = 0, Armor1 = 0, Attack1 = 0, Evasion2 = 0, Armor2 = 0, Attack2 = 0;
        int Health_Point1 = 0, Health_Point2 = 0;
	    Scanner input_str = new Scanner(System.in);
	    Scanner input_int = new Scanner(System.in);
        Robot robot = new Robot();
	    Robot.Robot_Petarung robot1 = new Robot().new Robot_Petarung();
	    Robot.Robot_Petarung robot2 = new Robot().new Robot_Petarung();
	    System.out.println("===== Battle Robot Fantasy - Beta V0.1 ======");
        System.out.println("========= By : Yudha Primadiansyah ==========\n\n");
        System.out.print("Masukan Nama Robot 1 = ");
        nama1 = input_str.nextLine();
        System.out.print("Masukan HP untuk Robot 1 = ");
        Health_Point1 = input_int.nextInt();
        System.out.print("Masukan Evasion untuk Robot 1 = ");
        Evasion1 = input_int.nextInt();
        System.out.print("Masukan Armor untuk Robot 1 = ");
        Armor1 = input_int.nextInt();
        System.out.print("Masukan Attack Point untuk Robot 1 = ");
        Attack1 = input_int.nextInt();
        robot.setRobotPetarung(robot1, nama1, Health_Point1, Evasion1, Armor1, Attack1);
        System.out.print("Masukan Nama Robot 2 = ");
        nama2 = input_str.nextLine();
        System.out.print("Masukan HP untuk Robot 2 = ");
        Health_Point2 = input_int.nextInt();
        System.out.print("Masukan Evasion untuk Robot 2 = ");
        Evasion2 = input_int.nextInt();
        System.out.print("Masukan Armor untuk Robot 2 = ");
        Armor2 = input_int.nextInt();
        System.out.print("Masukan Attack Point untuk Robot 2 = ");
        Attack2 = input_int.nextInt();
        robot.setRobotPetarung(robot2, nama2, Health_Point2, Evasion2, Armor2, Attack2);

        while(robot1.Health_Point > 0 && robot2.Health_Point > 0){
            Random ev1 = new Random();
            Random ev2 = new Random();
            System.out.println(robot2.nama+" Mendapat Serangan Dari "+robot1.nama+" Sebesar = "+robot1.Attack+" Sisa HP = "+robot.Attack(robot2, robot1.Attack, ev2.nextInt(robot2.Evasion)));
            System.out.println(robot1.nama+" Mendapat Serangan Dari "+robot2.nama+" Sebesar = "+robot2.Attack+" Sisa HP = "+robot.Attack(robot1, robot2.Attack, ev1.nextInt(robot1.Evasion)));
            robot.Profile(robot1);
            robot.Profile(robot2);
            robot.CekPemenang(robot1, robot2);
            System.out.println("Tekan Enter untuk melanjutkan Pertarungan");
            input_str.nextLine();
        }
    }


}
