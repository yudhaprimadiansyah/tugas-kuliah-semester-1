package com.robofight;
import java.util.Random;
import java.util.Scanner;
public class Robot {

    class Robot_Petarung {
        String nama;
        int Health_Point;
        int Evasion;
        int Armor;
        int Attack;
    }

    public void setRobotPetarung(Robot_Petarung robotnya, String nama, int HP, int EV, int ARM, int ATT){
        Scanner input = new Scanner(System.in);
        robotnya.nama = nama;
        robotnya.Health_Point = HP;
        robotnya.Evasion = EV;
        robotnya.Armor = ARM;
        robotnya.Attack = ATT;
    }

    public double Attack(Robot_Petarung robotnya_musuhnya, int hit, int evasion){
        if(robotnya_musuhnya.Health_Point % 3 == 0){
            return robotnya_musuhnya.Health_Point -= (hit + ((hit/100)*120)) - ((hit/100)*evasion) - robotnya_musuhnya.Armor;
        }
        else {
            return robotnya_musuhnya.Health_Point -= hit - ((hit/100)*evasion) - robotnya_musuhnya.Armor;
        }

    }

    public void Profile(Robot_Petarung robot){
        System.out.println("Nama Robot : "+robot.nama);
        if(robot.Health_Point % 3 == 0){
            System.out.println("Total HP : "+robot.Health_Point+" Terkena Critical Hit");
        }
        else {
            System.out.println("Total HP : "+robot.Health_Point);
        }
        System.out.println("Total Evasion : "+robot.Evasion);
        System.out.println("Total Armor : "+robot.Armor);
        System.out.println("Total Attack : "+robot.Attack);
    }

    public void CekPemenang(Robot_Petarung robot1, Robot_Petarung robot2){
        if(robot1.Health_Point < robot2.Health_Point){
            System.out.println(robot2.nama+" Unggul Dalam Fase kali ini");
        }

        if(robot1.Health_Point > robot2.Health_Point){
            System.out.println(robot1.nama+" Unggul Dalam Fase kali ini");
        }
    }


}
