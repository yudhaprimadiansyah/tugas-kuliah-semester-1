class MencariKetinggian {
	public static void Cari_Ketinggian(double v0, double g, double t){
		double h;
		h = (v0 * t) - ((1/2) * (g * Math.pow(t, 2)));
		System.out.println("Ketinggian Yang Di capai Benda dengan kecepatan awal " + v0 +" m/s dan waktu " + t + " Detik adalah = " + h + " Meter"); 
	}
	public static void main(String []args){
		double v0 = 20, g = 20, t = 5;
		Cari_Ketinggian(v0, g, t);
	}
}
