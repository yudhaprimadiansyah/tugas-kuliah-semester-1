import java.util.Scanner;
import java.util.Arrays;
class TP_MOD_9 {
	public static void selection(int arey[]){
		for(int i=0;i<arey.length - 1;i++){
			int max = i;
			for(int j=i+1;j<arey.length;j++){
				if(arey[j] < arey[max]){
					max = j;
				}
			}
			int temp = arey[max];
			arey[max] = arey[i];
			arey[i] = temp;
		}
		System.out.println(Arrays.toString(arey));
	}
	
	public static void Sorting(int arey[]){
			int temp = 0;
			for(int i=0;i<arey.length;i++){
				for(int j=1;j<(arey.length);j++){
					if(arey[j-1] > arey[j]){
						temp = arey[j-1];
						arey[j-1] = arey[j];
						arey[j] = temp;
					}
				}
			}
			System.out.println(Arrays.toString(arey));
	}
	public static void main(String []args){
	
		Scanner inp = new Scanner(System.in);
		System.out.print("Masukan Jumlah Data = ");
		int urutan[] = new int[inp.nextInt()];
		for(int i=0;i<urutan.length;i++){
			System.out.print("Masukan Data Ke "+(i+1)+" = ");
			int data = inp.nextInt();
			urutan[i] = data;
		}
		System.out.println("Data Sebelum Di Urutkan = "+Arrays.toString(urutan));
		System.out.println("----------Hasil Akhir Setelah Di Urutkan----------");
		Sorting(urutan);
		//selection(urutan);
	}
}
