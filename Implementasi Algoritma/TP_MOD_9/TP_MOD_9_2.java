import java.util.Scanner;
import java.util.ArrayList;
class TP_MOD_9_2 {
	static String nama;
	static String nim;
	static String nilai;
	static Scanner inp = new Scanner(System.in);
	static Scanner inp_int = new Scanner(System.in);
	static ArrayList<TP_MOD_9_2> mahasiswa = new ArrayList<TP_MOD_9_2>();
	
	public static void tambah_data(TP_MOD_9_2 mahastudent){
		
		System.out.print("Masukan Nama Mahasiswa = ");
		mahastudent.nama = inp.nextLine();
		System.out.print("Masukan Nomor Induk Mahasiswa = ");
		mahastudent.nim = inp.nextLine();
		System.out.print("Masukan Nilai Mahasiswa = ");
		mahastudent.nilai = inp.nextLine();	
		mahasiswa.add(mahastudent);
		
		System.out.println("Data Berhasil Di tambahkan");
	}
	
	public static void lihat_data(){
		for(int i=0;i<mahasiswa.size();i++){
			System.out.println("----------------");
			System.out.println("Nama	: "+mahasiswa.get(i).nama);
			System.out.println("NIM		: "+mahasiswa.get(i).nim);
			System.out.println("Nilai	: "+mahasiswa.get(i).nilai);
			System.out.println("----------------");
		}
	}

	public static void main(String []args){
		
		while(true){
			TP_MOD_9_2 mahastudent = new TP_MOD_9_2();
			System.out.println("==== Program Rekap Nilai Mahasiswa ====");
			System.out.println("1. Masukan Data\n2. Lihat Data\n3. Cari Data\n4.Exit");
			System.out.print("Masukan Pilihan Anda = ");
			int pilihan = inp_int.nextInt();
			switch(pilihan){
				case 1:
					tambah_data(mahastudent);
					break;
				case 2:
					lihat_data();
					break;
				case 3:
					//cari_data();
					break;
				case 4:
					System.exit(0);
					break;
				default:
					System.out.println("Pilihan Tidak Ada");
					break;
			}
			
		}
	}

}
