package com.looping;

import java.util.Collections;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        int i, i2, i3, start = 0;
        Scanner inputan = new Scanner(System.in);
        System.out.println("----Segitiga Maker----");
        System.out.println("Nama            : Yudha Primadiansyah");
        System.out.println("Kelas           : D3IF-42-02");
        System.out.println("NIM             : 6706180083\n");
        System.out.print("Masukan Jumlah Bilangan\t: ");
        start = inputan.nextInt();
        String bintang = "*";
        String spasi = " ";
        for(i=start; i>=1;i--){
            if(i == start || i==1){
                System.out.print(String.join("", Collections.nCopies(i, bintang)));
            }
            else {
                System.out.print(bintang+(String.join("", Collections.nCopies(i-2, spasi)))+bintang);
            }
            System.out.println("");
        }
    }
}
