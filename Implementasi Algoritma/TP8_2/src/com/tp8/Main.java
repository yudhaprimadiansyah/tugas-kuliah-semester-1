package com.tp8;

import java.util.Scanner;
import java.util.ArrayList;

class Main {
    static Scanner plewInt = new Scanner(System.in);
    static Scanner plewString = new Scanner(System.in);
    static ArrayList<String> Nama = new ArrayList<String>();
    static ArrayList<String> NIK = new ArrayList<String>();
    static ArrayList<String> Jabatan = new ArrayList<String>();

    public static void tambah_data(){

        String nama = "", nik = "", jabatan = "";
        if(Nama.size()+1 > 10){
            System.out.println("Error");
        }
        else {
            System.out.print("Masukan NIK : ");
            nama = plewInt.nextLine();
            System.out.print("Masukan NAMA : ");
            nik = plewString.nextLine();
            System.out.print("Masukan JABATAN : ");
            jabatan = plewString.nextLine();
            Nama.add(nama);
            NIK.add(nik);
            Jabatan.add(jabatan);
        }
    }

    public static void lihat_data(){
        for(int i=0;i<Nama.size();i++){
            System.out.println("NIK		: "+NIK.get(i));
            System.out.println("Nama	: "+Nama.get(i));
            System.out.println("Jabatan	: "+Jabatan.get(i));
            System.out.println("Gaji	: "+gaji(Jabatan.get(i)));
            System.out.println("------------------------------------");
        }
    }

    public static int gaji(String jabatan){
        int gaji = 0;
        if(jabatan.toLowerCase().equals("manajer")){
            gaji = 2000000;
        }
        else if(jabatan.toLowerCase().equals("hrd")){
            gaji = 1500000;
        }
        else if(jabatan.toLowerCase().equals("karyawan")){
            gaji = 1000000;
        }
        else {
            gaji = 0;
        }
        return gaji;
    }

    public static void main(String []args){
        int pilihan = 0;
        Scanner inp = new Scanner(System.in);
        while(true){
            System.out.println("=======PEGAWAI PABRIK MICIN=======");
            System.out.println("\t1. Tambah Pegawai");
            System.out.println("\t2. Lihat Pegawai");
            System.out.println("\t3. Exit");
            System.out.print("Masukan Pilihan Anda : ");
            pilihan = inp.nextInt();
            switch(pilihan){
                case 1:
                    tambah_data();
                    break;
                case 2:
                    lihat_data();
                    break;
                case 3:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Pilihan Tidak Ada");
                    break;

            }

        }
    }
}
