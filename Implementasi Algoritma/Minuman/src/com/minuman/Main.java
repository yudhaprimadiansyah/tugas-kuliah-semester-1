package com.minuman;

import java.util.Scanner;
class Main {

    public static void main(String []args){
        int uang = 0,harga = 0, error = 0, kembalian = 0, kurang = 0;
        Scanner inputan = new Scanner(System.in);
        //Scanner inputan_string = new Scanner(System.in);
        System.out.print("Uang	: ");
        uang = inputan.nextInt();
        System.out.print("Minuman	: ");
        String minuman = inputan.next();
        if(minuman.equals("fanta")){
            harga = 5000;
        }
        else if(minuman.equals("chatime")){
            harga = 30000;
        }
        else if(minuman.equals("amidis")){
            harga = 15000;
        }
        else {
            error = 1;
        }
        if(error == 0){
            System.out.println("Harga	: " + harga);
            if(uang >= harga){
                kembalian = uang - harga;
                System.out.println("Transaksi Untuk Minuman " + minuman + " Berhasil !!! Kembalian Rp. " + kembalian);
            }
            else {
                kurang = harga - uang;
                System.out.println("Uang Kamu Kurang Rp. " + kurang + " Untuk Membeli " + minuman + " Silahkan Tambah Uang Anda ");
            }
        }
        else {
            System.out.println("Barang " +minuman+" yang anda Cari Tidak Ada!");
        }
    }


}

