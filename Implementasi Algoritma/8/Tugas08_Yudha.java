import java.util.Scanner;
/* Aplikasi Konversi Biner Desimal dan implementasi Caesar Cipher Encryption, mempelajari mekanisme cryptography dasar dalam java*/
class Tugas08_Yudha {

	static Scanner inputan = new Scanner(System.in);
	public static void Dec2Bin(int angka){
		int sisa, total = angka;
		StringBuffer jadi_biner = new StringBuffer();
		while(total>=1){
			sisa = total % 2;
			if(sisa == 1){
				total = (total-sisa)/2;
				jadi_biner.append("1");
			}
			else if(sisa == 0){
				total = total/2;
				jadi_biner.append("0");
			}
			else if(total == 0 | total == 1){
				jadi_biner.append(total);
			}
		}
		System.out.println("\nNilai Biner dari " + angka + " Adalah = " + jadi_biner.reverse());
	}

	public static void Bin2Dec(String biner){
		int i, error = 0, desimal = 0;
		StringBuffer jadi_biner = new StringBuffer();
		StringBuffer dibalik;
		StringBuffer jadibaper = new StringBuffer(biner);
		dibalik = jadibaper.reverse();
		String karakter;
		for(i=0;i<=(dibalik.length()-1);i++){
			karakter = Character.toString(dibalik.charAt(i));
			if(karakter.equals("1")){
				desimal += Math.pow(2, i);
			}
			else if(karakter.equals("0")){
				continue;
			}
			else {
				System.out.println("\nAnda Memasukan Nilai Selain 0 dan 1");
				error = 1;
				break;
			}
		}
		if(error == 0){
			System.out.println("\nNilai Desimal Dari " + biner + " Adalah = " + desimal);
		}

	}

	public static String EnskripsiTeks(String Teks){
			int i;
			String Teks_Upper = Teks.toUpperCase();
			StringBuffer plaintext = new StringBuffer(Teks_Upper);
			StringBuffer encrypted_teks = new StringBuffer();
			char new_char;
			for(i=0;i<=plaintext.length()-1;i++){
				if(plaintext.charAt(i) == ' '){
					encrypted_teks.append(' ');
				}
				else {
					new_char = (char)((int)plaintext.charAt(i) + 13);
					if((int)new_char > 90){
						new_char = (char)((int)new_char - 26);
						encrypted_teks.append(Character.toString((char)new_char));
					}
					else if((int)new_char < 65){
						new_char = (char)((int)new_char + 26);
						encrypted_teks.append(Character.toString((char)new_char));
					}
					else {
						encrypted_teks.append(Character.toString((char)new_char));
					}
				}
			}
			//System.out.println("\nNew Encrpted Teks = " + encrypted_teks);
			String hasil = encrypted_teks.toString();
			return hasil;
	}

	public static String DekripsiTeks(String Teks){
		int i;
		String Teks_Upper = Teks.toUpperCase();
		StringBuffer plaintext = new StringBuffer(Teks_Upper);
		StringBuffer decrypted_teks = new StringBuffer();
		char new_char;
		for(i=0;i<=plaintext.length()-1;i++){
			if(plaintext.charAt(i) == ' '){
				decrypted_teks.append(' ');
			}
			else {
				new_char = (char)((int)plaintext.charAt(i) - 13);
				if((int)new_char > 90){
					new_char = (char)((int)new_char - 26);
					decrypted_teks.append(Character.toString((char)new_char));
				}
				else if((int)new_char < 65){
					new_char = (char)((int)new_char + 26);
					decrypted_teks.append(Character.toString((char)new_char));
				}
				else {
					decrypted_teks.append(Character.toString((char)new_char));
				}
			}
		}
		//System.out.println("\nNew Decrypted Teks = " + decrypted_teks);
		String hasil = decrypted_teks.toString();
		return hasil;
	}

	public static void main(String []args){
		Scanner string_data = new Scanner(System.in);
		int pilihan, angka, attempt = 3;
		String passwd = "LHQUNCEVZN", passinpt, username = "yudha";
		boolean ulang = true;
		String Teks;
		String biner;
		while(attempt > 0){
		System.out.print("Masukan Username = ");
		username = string_data.nextLine();
		System.out.print("Masukan Password = ");
		passinpt = string_data.nextLine();
		String enkripted = EnskripsiTeks(passinpt);
		if(enkripted.equals(passwd) && username.equals("yudha")){
			System.out.println("Welcome Home, Yudha");
			while(ulang==true){
			System.out.println("\n\n==== Program Konversi Binary - Decimal & implementasi Caesar ROT13 Cipher ====");
			System.out.print("1. Binary To Decimal\n2. Decimal to Binary\n3. Encrypt Teks to ROT13\n4. Decrypt from ROT13\n5. Exit\n Masukan Pilihan Anda = ");
			pilihan = inputan.nextInt();
			switch(pilihan){
				case 1:
					System.out.print("Masukan Nilai Biner = ");
					biner = string_data.nextLine();
					Bin2Dec(biner);
					break;
				case 2:
					System.out.print("Masukan Nilai Desimal = ");
					angka = inputan.nextInt();
					Dec2Bin(angka);
					break;
				case 3:
					System.out.print("Masukan Teks Yang ingin Di Enskripsi = ");
					Teks = string_data.nextLine();
					System.out.println("Hasil Dari Enskripsi = " + EnskripsiTeks(Teks));
					break;
				case 4:
					System.out.print("Masukan Teks Yang ingin Di Dekripsi = ");
					Teks = string_data.nextLine();
					System.out.println("Hasil Dari Dekripsi = " + DekripsiTeks(Teks));
					break;
				case 5:
					System.out.println("Keluar.....");
					ulang = false;
					System.exit(0);
					break;
				default:
					System.out.println("Pilihan Tidak Ada");
					break;
				}
			}
		}
		else {
			attempt--;
			System.out.println("OOppssss Salaahhhh Anda Memiliki "+attempt+" Kesempatan Lagi");
		}
		if(attempt == 0){
			System.out.println("Anda Tidak Mempunyai Kesempatan Login Lagi, Bye");
		}
	}
}
}
