package com.segitiga;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    double alas, tinggi;
	    double luas_segitiga, keliling_segitiga;


	    System.out.println("\t Rumus Segitiga Sama Sisi ");
		System.out.println(" ================================");
	    Scanner inputan = new Scanner(System.in);
	    System.out.print("Masukan Panjang Alas	= ");
	    alas = inputan.nextInt();
        System.out.print("Masukan Tinggi			= ");
        tinggi = inputan.nextInt();
        luas_segitiga = (alas * tinggi) * ((float)1/2);
        keliling_segitiga = 3 * alas;
		System.out.println("Luas Segitiga			= 1/2 * " + alas + " * " + tinggi + " = " + luas_segitiga );
		System.out.println("Keliling Segitiga		= 3 " + " * " + alas + " = " + keliling_segitiga);

    }
}
