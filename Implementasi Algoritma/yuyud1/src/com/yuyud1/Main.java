package com.yuyud1;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int pilihan, uang;
        while(true) {
            Warung warong = new Warung();
            System.out.println("======Program Nasi Padang=====");
            Scanner inputan = new Scanner(System.in);
            System.out.println("1. Pesan\n2. Bayar\n3. Keluar\n" +
                     "Total Pembayaran = " + warong.total + "\n" +
                    "Masukan Pilihan Anda = ");
            pilihan = inputan.nextInt();
            if(pilihan == 1){
                warong.pesan();
            }
            else if(pilihan == 2){
                System.out.println("Masukan Jumlah Uang Yang Akan anda Bayarkan = ");
                uang = inputan.nextInt();
                warong.bayar(uang);
            }
            else if(pilihan == 3){
                break;
            }
            else {
                System.out.println("Pilihan Tidak Ada");
            }
        }
    }
}

class Warung {
    static int total = 0;
    public static void pesan(){
        while(true){
            int pilihan;
            System.out.println("1. Nasi + Rendang\n" +
                    "2. Nasi + Ikan\n" +
                    "3. Nasi + Ayam Pop\n" +
                    "4. Kembali\n" +
                    "\n" +
                    "Total Pembayaran Anda " + total +
                    "\nMasukan Pilihan Anda = ");
            Scanner inputan = new Scanner(System.in);
            pilihan = inputan.nextInt();
            if(pilihan == 1){
                total += 15000;
            }
            else if(pilihan == 2){
                total += 14000;
            }
            else if(pilihan == 3){
                total += 12000;
            }
            else if(pilihan == 4){
                break;
            }
            else {
                System.out.println("Pilihan Tidak Ada");
            }
        }
    }

    public static void bayar(int bayar){

        if(bayar <= total){
            System.out.println("Bayarnya Kurang Mas, mohon membayar lagi");
        }
        else if(bayar > total){
            int kembalian;
            kembalian = bayar - total;
            System.out.println("Anda Mendapatkan Kembalian Rp. " + kembalian );
            total = 0;
        }
        else if(bayar == total){
            System.out.println("Anda Membayar dengan Bayaran yang pas, Terimakasih");
            total = 0;
        }
    }
}