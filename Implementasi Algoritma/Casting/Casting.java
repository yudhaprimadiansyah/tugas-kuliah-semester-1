class Casting {
	public static void main(String []args){
		double belum_jadi = 3.14;
		int jadi_int = (int)belum_jadi;
		char a = '*';
		int char_jadi_int = a;

		System.out.println("Sebelum Casting = " + belum_jadi);
		System.out.println("Setelah casting dengan int = " + (int)jadi_int);
		System.out.println("Setelah casting dengan char = " + char_jadi_int);
	}

}
