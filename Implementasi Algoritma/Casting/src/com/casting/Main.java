package com.casting;

public class Main {

    public static void main(String[] args) {
	    double sebelum_casting = 3.14;
	    int casting_ke_int = (int)sebelum_casting;
	    char karakter = '*';
	    int jadi_int = (int)karakter;

	    System.out.println("Sebelum Casting				: " + sebelum_casting);
	    System.out.println("Setelah Casting dengan int	: " + casting_ke_int);
	    System.out.println("Setelah Casting	dengan char	: " + jadi_int);
    }
}
