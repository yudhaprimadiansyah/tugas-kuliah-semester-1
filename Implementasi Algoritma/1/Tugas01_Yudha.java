/* Program Konversi Binary Ke Desimal algoritma pemikiran sendiri dengan variable statis, penjelasan tiap line disisipkan di program versi 5*/
class Tugas01_Yudha {
	
	public static void main(String []args){
		int i = 0;
		String biner = "110011";
		StringBuffer dibalik;
		StringBuffer jadibaper = new StringBuffer(biner);
		dibalik = jadibaper.reverse();
		String karakter;
		int desimal = 0, error = 0;
		for(i=0;i<=(dibalik.length()-1);i++){
			karakter = Character.toString(dibalik.charAt(i));
			
			if(karakter.equals("1")){
				desimal += Math.pow(2, i);
			}
			else if(karakter.equals("0")){
				continue;
			}
			else {
				System.out.println("Anda Memasukan Nilai Selain 0 dan 1");
				error = 1;
				break;
			}
		}
		if(error == 0){
			System.out.println("Nilai Desimal Dari " + biner + " Adalah = " + desimal);
		}
	}
}
