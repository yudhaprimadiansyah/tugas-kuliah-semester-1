import java.util.Scanner;

class TP3_2 {
	public static void main(String []args){
		int day;
		String hari;
		Scanner inputan = new Scanner(System.in);
		System.out.print("Masukan Angka (1-7) = ");
		day = inputan.nextInt();
		switch(day){
			case 1:
				hari = "Monday";
				break;
			case 2:
                                hari = "Tuesday";
                                break;
			case 3:
                                hari = "Wednesday";
                                break;
			case 4:
                                hari = "Thursday";
                                break;
			case 5:
                                hari = "Friday";
                                break;
			case 6:
                                hari = "Saturday";
                                break;
			case 7:
                                hari = "Sunday";
                                break;
			default:
                                hari = "Tidak Ada :)";
                                break;
			}
			System.out.println("Hari Ke-"+ day +" Adalah Hari " + hari);
	}

}
