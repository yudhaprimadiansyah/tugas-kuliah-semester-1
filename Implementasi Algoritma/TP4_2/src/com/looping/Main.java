package com.looping;

import java.util.Scanner;

public class Main {
    static Scanner inputan = new Scanner(System.in);
    public static void Lihat_Menu(){
        System.out.println("\n\ta. Indomie\n\tb. Baso\n\tc. Nasgor");
    }

    public static void Pesan_Makanan(){
        int meja, total_bayaran, porsi;
        String pesanan;
        System.out.print("Nomor Meja        : ");
        meja = inputan.nextInt();
        System.out.print("Pesanan           : ");
        pesanan = (inputan.next()).toLowerCase();

        switch(pesanan){
            case "indomie":
                System.out.println("Anda Memesan Indomie, Harga Rp. 5.000");
                break;
            case "bakso":
                System.out.println("Anda Memesan Baso, Harga Rp. 10.000");
                break;
            case "nasgor":
                System.out.println("Anda Memesan Nasi Goreng, Harga Rp. 11.000");
                break;
            default:
                System.out.println("Pesanan Anda Tidak Tersedia");
                break;
        }
    }
    public static void main(String[] args){
        boolean ulang = true;
        int pilihan;
        System.out.println("---Selamat Datang di Kantin Sehat---\n");
        System.out.println("Nama            : Yudha Primadiansyah");
        System.out.println("Kelas           : D3IF-42-02");
        System.out.println("NIM             : 6706180083");
        do {
            System.out.println("\n\t1. Lihat Menu\n\t2. Pesan Makanan\n\t3. Keluar");
            System.out.print("Pilihan Anda          : ");
            pilihan = inputan.nextInt();

            switch(pilihan){
                case 1:
                    Lihat_Menu();
                    break;
                case 2:
                    Pesan_Makanan();
                    break;
                case 3:
                    System.out.println("Terima kasih telah berkunjung ke kantin sehat :)");
                    ulang = false;
                    break;
                default:
                    System.out.println("Pilihan Tidak Ada");
            }

        }while(ulang == true);
    }

}
