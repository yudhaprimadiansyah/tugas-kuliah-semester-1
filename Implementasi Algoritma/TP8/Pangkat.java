import java.util.Scanner;

class Pangkat {
	int pangkatin(int x, int y){
	if(y>0){
		return x * pangkatin(x, y-1);
	}
	else {
		return 1;
	}
}

public static void main(String args[]){
		Scanner inp = new Scanner(System.in);
		int a = 0, b = 0;
		System.out.print("Masukan Angka = ");
		a = inp.nextInt();
		System.out.print("Masukan Pangkat = ");
		b = inp.nextInt();
		Pangkat pkt = new Pangkat();
		System.out.println("Hasil Pangkat "+a+" Pangkat "+b+" = "+pkt.pangkatin(a, b));
	}
	
}
