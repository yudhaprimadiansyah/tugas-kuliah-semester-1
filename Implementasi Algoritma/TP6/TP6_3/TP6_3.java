import java.util.Scanner;
import java.util.ArrayList;
class TP6_3 {
	public static void main(String []args){
		int inputan = 0;
		Scanner input_int = new Scanner(System.in);
		Scanner input_str = new Scanner(System.in);
		ArrayList<String> nama_mhs = new ArrayList<String>();
		while(true){
			String nama = "";
			System.out.println("=== Program Pendataan Mahasiswa ==="+
			"\nMenu :"+
			"\n1. Tambahkan Data\n2. Tampilkan Data\n3. Keluar\nPilihan Anda = ");
			inputan = input_int.nextInt();
			switch(inputan){
				case 1:
					System.out.print("Masukan Nama = ");
					nama = input_str.nextLine();
					nama_mhs.add(nama);
					break;
				case 2:
					System.out.println("\nData Saat Ini :");
					for(int i = 0;i<=nama_mhs.size()-1;i++){
						System.out.println((i+1)+". "+nama_mhs.get(i));
					}
					System.out.println("\n\n");
					break;
				case 3:
					System.exit(0);
					break;
			}
		}
	}
}
