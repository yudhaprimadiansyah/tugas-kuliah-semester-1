/* aplikasi konversi Biner <-----> Desimal dengan Algoritma Pemikiran sendiri, Scanner Included */
import java.util.Scanner;
class Tugas03_Yudha {
	
	public static void main(String []args){
		int i = 0, desimal = 0, error = 0;
		int sisa, angka = 0;
		boolean ulang = true;
		String biner;
		Scanner inputan = new Scanner(System.in);
		System.out.print("Masukan Nilai Biner = ");
		biner = inputan.nextLine();
		System.out.print("Masukan Nilai Desimal = ");
		angka = inputan.nextInt();
		int total = angka;
		StringBuffer jadi_biner = new StringBuffer(); 
		StringBuffer dibalik;
		StringBuffer jadibaper = new StringBuffer(biner);
		dibalik = jadibaper.reverse();
		String karakter;
		for(i=0;i<=(dibalik.length()-1);i++){
			karakter = Character.toString(dibalik.charAt(i));
			if(karakter.equals("1")){
				desimal += Math.pow(2, i);
			}
			else if(karakter.equals("0")){
				continue;
			}
			else {
				System.out.println("Anda Memasukan Nilai Selain 0 dan 1");
				error = 1;
				break;
			}
		}
		if(error == 0){
			System.out.println("Nilai Desimal Dari " + biner + " Adalah = " + desimal);
		}
		
		while(total>=1){
			sisa = total % 2;
			if(sisa == 1){
				total = (total-sisa)/2;
				jadi_biner.append("1");
			}
			else if(sisa == 0){
				total = total/2;
				jadi_biner.append("0");
			}
			else if(total == 0 | total == 1){
				jadi_biner.append(total);
			}
		}
		System.out.println("Nilai Biner dari " + angka + " Adalah = " + jadi_biner.reverse());
	}
}
